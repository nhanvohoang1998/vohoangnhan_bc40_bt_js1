// Bài 1: Tính tiền lương nhân viên
/**
 * input: số ngày làm và lương cố định là 100.000
 * 
 * Xử lý: tính lương = số ngày làm việc * lương cố định
 * 
 * output: kết quả lương thu được với số ngày làm việc nhập vào
 */

const LUONG = 100.000;
var soNgay = 10;  //ví dụ số ngày lương là 10
var tinhLuong = soNgay * LUONG;
console.log("Bài 1: \n Lương nhân viên nhận được với", soNgay , "ngày làm việc là:", tinhLuong, "\n");


// Bài 2: Tính giá trị trung bình
/**
 * input: nhập 5 số thực như ví dụ bên dưới
 * 
 * Xử lý: Cộng 5 số thực trước rồi chia 5 để lấy giá trị trung bình
 * 
 * output: Tính ra kết quả giá trị trung bình của 5 số thực đã nhập
 */

var soThuc1 = 1;
var soThuc2 = 2;
var soThuc3 = 3;
var soThuc4 = 4;
var soThuc5 = 5;
var giaTriTB = (soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5) / 5 ;
console.log("Bài 2: \n Giá trị trung bình:", giaTriTB, "\n");


// Bài 3: Quy đổi tiền
/**
 * input: Nhập số usd cần đổi và trị giá 23.500 1 usd sang vnd
 * 
 * Xử lý: lấy số usd cần đổi nhân với trị giá
 * 
 * output: Số tiền nhận được từ usd sang vnđ được xử lý ở công thức trên
 */

const USD_TO_VND = 23.500;
var soUSD = 2;
var soTienVND = soUSD * USD_TO_VND;
console.log("Bài 3: \n Người dùng nhập", soUSD, "USD =>", soTienVND, "VND \n")

// Bài 4: Tính diện tích, chu vi hình chữ nhật
/**
 * input: nhập vào chiều dài, chiều rộng
 * 
 * xử lý: diện tích = dài * rộng, chu vi = (dài + rộng) *2
 * 
 * output: tính ra chu vi và diện tích từ chiều dài rộng nhập vào
 */

var chieuDai = 4;
var chieuRong = 2;
var chuVi = (chieuDai + chieuRong) *2;
var dienTich = chieuDai * chieuRong;
console.log("Bài 4: \n Diện tích:", dienTich, "\n Chu vi:", chuVi, "\n")

// Bài 5: Tính tổng 2 ký số
/**
 * input: nhập vào số có 2 ký số
 * 
 * xử lý: lấy số hàng chục bằng cách chia lấy phần nguyên, lấy số hàng đơn vị bằng cách chia lấy phần dư -> tính tổng 2 chữ số đã tách được
 * 
 * output: tổng 2 chữ số sau khi tách
 */

var nhapSo = 89;
var soThu1 = nhapSo / 10;
var soThu2 = nhapSo % 10;
var tong2KySo = Math.floor(soThu1) + soThu2;
console.log("Bài 5: \n Tổng 2 ký số:", tong2KySo)